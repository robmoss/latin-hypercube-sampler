import numpy as np
import pytest
import scipy.stats

import lhs
import lhs.sample


def make_rng():
    return np.random.default_rng(seed=20201217)


def test_draw_reproducible():
    """
    Ensure that reproducible draws are returned when the distributions are
    defined with a consistent ordering.
    """
    n = 50
    dists_xy = {
        'x': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
        'y': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
    }
    dists_xyz = {
        'x': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
        'y': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
        'z': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
    }
    dists_zxy = {
        'z': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
        'x': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
        'y': {
            'name': 'uniform',
            'args': {'loc': 0, 'scale': 1},
        },
    }
    samples_xy = lhs.draw(make_rng(), n, dists_xy)
    samples_xyz = lhs.draw(make_rng(), n, dists_xyz)
    samples_zxy = lhs.draw(make_rng(), n, dists_zxy)
    for name, orig_values in samples_xy.items():
        # Should be identical, x is the first parameter for both.
        assert np.array_equal(orig_values, samples_xyz[name])
        # Should not be identical, x is the second parameter for samples_zxy.
        assert not np.array_equal(orig_values, samples_zxy[name])


def test_draw_independent():
    """
    Test drawing values for two independent parameters.
    """
    a_const = 5.678
    b_min = 10
    b_max = 20
    b_scale = b_max - b_min
    params = {
        'a': {
            'name': 'constant',
            'args': {
                'value': a_const,
            },
        },
        'b': {
            'name': 'uniform',
            'args': {
                'loc': b_min,
                'scale': b_scale,
            },
        },
    }

    rng = make_rng()
    n = 20
    values = lhs.draw(rng, n, params)

    assert set(values.keys()) == {'a', 'b'}

    # Inspect the values returned for each parameter.
    values_a = values['a']
    assert values_a.shape == (n,)
    assert np.all(values_a == a_const)

    values_b = values['b']
    assert values_b.shape == (n,)
    assert np.all(values_b >= b_min)
    assert np.all(values_b <= b_max)
    assert np.any(values_b < b_min + b_scale / n)
    assert np.any(values_b > b_max - b_scale / n)


def test_draw_invalid_deps():
    rng = make_rng()
    n = 20
    params = {}

    with pytest.raises(ValueError):
        lhs.draw(rng, n, params, dep_params={}, dep_fn=None)

    with pytest.raises(ValueError):
        lhs.draw(rng, n, params, dep_params=None, dep_fn=map)


def test_draw_dependent_parameters():
    """
    Test drawing values for two independent parameters and one dependent
    parameter.
    """
    a_const = 5.678
    b_min = 10
    b_max = 20
    b_scale = b_max - b_min
    params = {
        'a': {
            'name': 'constant',
            'args': {
                'value': a_const,
            },
        },
        'b': {
            'name': 'uniform',
            'args': {
                'loc': b_min,
                'scale': b_scale,
            },
        },
    }

    def dep_fn(indep_values, dep_params):
        """
        Define ``c = 2b``.
        """
        b_values = indep_values['b']
        return {
            'c': {'ppf': lambda qtls: 2 * b_values * np.ones(qtls.shape)},
        }

    dep_params = {'c': {'shape': 1}}

    rng = make_rng()
    n = 20
    values = lhs.draw(rng, n, params, dep_params, dep_fn)

    assert set(values.keys()) == {'a', 'b', 'c'}

    # Inspect the values returned for each parameter.
    values_a = values['a']
    assert values_a.shape == (n,)
    assert np.all(values_a == a_const)

    values_b = values['b']
    assert values_b.shape == (n,)
    assert np.all(values_b >= b_min)
    assert np.all(values_b <= b_max)
    assert np.any(values_b < b_min + b_scale / n)
    assert np.any(values_b > b_max - b_scale / n)

    values_c = values['c']
    assert values_c.shape == (n,)
    assert np.array_equal(values_c, 2 * values_b)


def test_draw_invalid_dependent_parameters():
    """
    Test drawing values for two independent parameters and one invalid
    dependent parameter.
    """
    a_const = 5.678
    b_min = 10
    b_max = 20
    b_scale = b_max - b_min
    params = {
        'a': {
            'name': 'constant',
            'args': {
                'value': a_const,
            },
        },
        'b': {
            'name': 'uniform',
            'args': {
                'loc': b_min,
                'scale': b_scale,
            },
        },
    }

    def dep_fn(indep_values, dep_params):
        """
        Define ``d = 2b`` rather than ``c = 2b``.
        """
        b_values = indep_values['b']
        return {
            'd': {'ppf': lambda qtls: 2 * b_values * np.ones(qtls.shape)},
        }

    dep_params = {'c': {'shape': 1}}

    rng = make_rng()
    n = 20
    with pytest.raises(ValueError):
        _ = lhs.draw(rng, n, params, dep_params, dep_fn)


def test_draw_uncorrelated():
    """
    Test that no two sampled parameters are perfectly correlated.
    """
    params = {
        'a': {
            'name': 'uniform',
            'args': {
                'loc': 0,
                'scale': 1,
            },
        },
        'b': {
            'name': 'uniform',
            'args': {
                'loc': 0,
                'scale': 1,
            },
        },
    }
    dep_params = {'c': {'shape': 1}}

    def dep_fn(indep_values, dep_params):
        return {
            'c': {
                'name': 'uniform',
                'args': {
                    'loc': 0,
                    'scale': 1,
                },
            },
        }

    rng = make_rng()
    n = 100
    values = lhs.draw(rng, n, params, dep_params, dep_fn)

    # Ensure that no two parameters are perfectly correlated.
    names = list(values.keys())
    for ix, name_a in enumerate(names[:-1]):
        for name_b in names[ix + 1 :]:
            corr = scipy.stats.pearsonr(values[name_a], values[name_b])[0]
            assert not np.isclose(abs(corr), 1)
