.. _install:

Installation
============

You must have Python 3.6, or later, installed.
On Windows, the simplest option is to use
`Anaconda <https://docs.continuum.io/anaconda/>`__.

You can install lhs_ with ``pip``. This is best done in a
`virtual environment
<http://docs.python-guide.org/en/latest/dev/virtualenvs/>`__.
It will also install any required packages that are not currently installed.

To install the lhs package:
   .. code-block:: shell

      pip install lhs
