.. _distributions:

Supported distributions
=======================

The ``lhs`` package allows distribution to be defined in several ways.
Here are examples of each way, for the uniform distribution on ``[8, 10]``:

1. Identifying the distribution **by name** (see below for supported distributions):

   .. code-block:: python

      dist_a = {'name': 'uniform', 'args': {'loc': 8, 'scale': 2}}

2. Providing a **frozen** ``scipy.stats`` **distribution**:

   .. code-block:: python

      import scipy.stats
      dist_b = {'distribution': scipy.stats.uniform(loc=8, scale=2)}

3. Providing the distribution's **percent point function** (inverse of the CDF):

   .. code-block:: python

      dist_c = {'ppf': lambda p: 8 + 2 * p}
      # Alternatively, via the scipy.stats distribution.
      dist_d = {'ppf': scipy.stats.uniform(loc=8, scale=2).ppf}

Distributions provided by lhs.dist
----------------------------------

The ``lhs.dist`` module defines the following distributions:

.. py:module:: lhs.dist

.. autofunction:: constant

.. autofunction:: inverse_uniform

Distributions provided by scipy.stats
-------------------------------------

All of the `scipy.stats`_ distributions can be identified by name, as well as by passing a distribution or its percent point function (inverse of the CDF).

.. note::

   Check the documentation for each `scipy.stats`_ distribution to identify the relevant distributions parameters and how their values are interpreted.

Consider the Beta distribution provided by `scipy.stats.beta <https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.beta.html>`__, which accepts the following parameters:

* Shape parameters ``a`` and ``b``, also known as :math:`\alpha` and :math:`\beta`;
* Shift parameter ``loc``, which defines the lower bound (default: ``0``); and
* Scale parameter ``scale``, which defines the range ``max - loc`` (default: ``1``).

We can define a Beta prior for the parameter ``a`` with shape parameters :math:`\alpha = 2` and :math:`\beta = 5`, over the interval :math:`[10, 15]`, with the following:

.. code-block:: python

   # Define the distribution for "a".
   a_dist = {'name': 'beta', 'args': {'a': 2, 'b': 5, 'loc': 10, 'scale': 5}}

Consider the discrete distribution `scipy.stats.randint <https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.randint.html>`__, which returns integers in the range ``[low, high - 1]`` with uniform probability.
We can define a prior for the parameter ``b`` that takes values between 1 and 10 with the following:

.. code-block:: python

   # Define the distribution for "b".
   b_dist = {'name': 'randint', 'args': {'low': 1, 'high': 11}}

We can then sample values from this distribution with :func:`lhs.sample.lhs_values` or :func:`lhs.dist.sample_from`:

.. doctest::

   >>> # Define the distribution for "b".
   >>> b_dist = {'name': 'randint', 'args': {'low': 1, 'high': 11}}
   >>> # Define the sample locations.
   >>> import numpy as np
   >>> samples = np.linspace(0.05, 0.95, 10)
   >>> # Obtain values with lhs.sample.lhs_values().
   >>> import lhs
   >>> values_1 = lhs.sample.lhs_values('b', b_dist, samples)
   >>> # Obtain values with lhs.dist.sample_from().
   >>> values_2 = lhs.dist.sample_from(samples, 'randint', b_dist['args'])
   >>> # These two arrays should contain identical values.
   >>> assert np.allclose(values_1, values_2)
   >>> # Both arrays should contain the integers 1 to 10 (inclusive).
   >>> assert np.allclose(values_1, np.arange(1, 11))
