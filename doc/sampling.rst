.. _sampling:

Drawing samples
===============

.. autofunction:: lhs.draw

Support functions
-----------------

.. autofunction:: lhs.sample.sample_subspace

.. autofunction:: lhs.sample.sample_subspaces

.. autofunction:: lhs.sample.lhs_values

.. autofunction:: lhs.dist.sample_from
